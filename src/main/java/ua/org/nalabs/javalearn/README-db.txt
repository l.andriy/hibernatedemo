створити базу даних так щоб була кирилиця (Debian, UTF-8):

CREATE DATABASE IF NOT EXISTS hiber_demo CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

GRANT ALL PRIVILEGES ON hiber_demo.* TO 'andriy'@'%';
FLUSH PRIVILEGES;

перевірити створення таблиці зі всіма параметрами:

show database create clinic;


select s.ID,s.FIRST_NAME, s.LAST_NAME from student as s, course as c, course_student as cs where c.TITLE = 'Professional swimming' and cs.COURSE_ID=c.ID and s.ID=cs.STUDENT_ID;
select s.ID,s.FIRST_NAME, s.LAST_NAME, c.TITLE from student as s, course as c, course_student as cs where c.TITLE = 'Professional swimming' and cs.COURSE_ID=c.ID and s.ID=cs.STUDENT_ID;
