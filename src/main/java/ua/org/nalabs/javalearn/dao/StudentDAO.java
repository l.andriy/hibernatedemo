package ua.org.nalabs.javalearn.dao;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import ua.org.nalabs.javalearn.entity.Student;

import javax.persistence.criteria.CriteriaBuilder;

public class StudentDAO {
    private Session session;

    public StudentDAO(Session session) {
        this.session = session;
    }

    public Student getStudent(String lastName) {
        Student student = null;
        try {
            Query query = session.getNamedQuery("findStudentByLastName");
            query.setParameter("lastName", lastName);
            student = (Student) query.uniqueResult();
            System.out.println(student);
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        return student;
    }

    public Student getStudent2(String lastName) {
        Student student = null;
        CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
        Transaction transaction = session.beginTransaction();
        return student;
    }

    public boolean studentExists(String lastName) {
        Student student = null;
        try {
            Query query = session.getNamedQuery("findStudentByLastName");
            query.setParameter("lastName", lastName);
            student = (Student) query.uniqueResult();
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        return student != null;
    }

    public void addStudent(Student student) {
        Transaction t = null;
        try {
            t = session.beginTransaction();
            session.save(student);
            t.commit();
        } catch (HibernateException e) {
            e.printStackTrace();
            t.rollback();
        }
    }

    public void deleteStudent(Student student) {
        Transaction t = null;
        try {
            t = session.beginTransaction();
            session.delete(student);
            t.commit();
        } catch (HibernateException e) {
            e.printStackTrace();
            t.rollback();
        }
    }

    public void updateStudent(Student student) {
        Transaction t = null;
        try {
            t = session.beginTransaction();
            session.update(student);
            t.commit();
        } catch (HibernateException e) {
            e.printStackTrace();
            t.rollback();
        }
    }
}

