package ua.org.nalabs.javalearn.dao;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import ua.org.nalabs.javalearn.entity.Course;
import ua.org.nalabs.javalearn.entity.TeacherAssessment;
import ua.org.nalabs.javalearn.entity.Student;

import java.util.Set;

public class SkillDAO {
    private Session session;
    public SkillDAO(Session session) {
        this.session = session;
    }
    public Set<Student> findRegistedOnCourse(String courseTitle) {
        Set<Student> registedOnCourse;
        Query query = session.createQuery("FROM Course WHERE title=:title");
        query.setParameter("title", courseTitle);
        Course course = (Course) query.uniqueResult();
        registedOnCourse = course.getStudents();
        return registedOnCourse;
    }
    public boolean addCourse(TeacherAssessment teacherAssessment) {
        boolean flag = false;
        Transaction t = null;
        try {
            t = session.beginTransaction();
            session.saveOrUpdate(teacherAssessment);
            t.commit();
            flag = true;
        } catch (HibernateException e) {
            e.printStackTrace();
            t.rollback();
        }
        return flag;
    }
}
