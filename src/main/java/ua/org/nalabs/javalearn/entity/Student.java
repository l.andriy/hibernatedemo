package ua.org.nalabs.javalearn.entity;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.GenericGenerator;

import static javax.persistence.EnumType.STRING;

@Entity
@Table(name = ("student"))
// HQL query
@NamedQueries({
        @NamedQuery(name = "findStudentByLastName",
                query = "select s from Student s where s.lastName = :lastName"),
        @NamedQuery(name = "findStudentBySex",
                query = "select s from Student s where s.sex = :sex")
})
// SQL query
@NamedNativeQueries({
        @NamedNativeQuery(
                name = "Student.findRandomStudent",
                query = "SELECT * FROM student s LIMIT 1",
                resultClass = Student.class
        )
})
public class Student implements Serializable {
    @Id
    @GenericGenerator(name="auto_inc", strategy = "increment")
    @GeneratedValue(generator="auto_inc")
    @Column(name = ("student_id"))
    private Long id;
    @Column(name = ("LAST_NAME"))
    @NotNull(message = "The last name must not be null")
    private String lastName;
    @Column(name = ("FIRST_NAME"))
    // hibernate validators
    // http://docs.jboss.org/hibernate/stable/validator/reference/en-US/html_single/#section-declaring-bean-constraints
    @NotNull(message = "The first name must not be null")
    @Size(min = 2, max = 15)
    private String firstName;

    @NotNull(message = "The email name must not be null")
    @Email
    private String email;

    private LocalDate birthday;
    @Column
    @Enumerated(value = STRING)
    private Sex sex;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "student")
    private Set<TeacherAssessment> teacherAssessments = new HashSet<>();

    @Transient
    private int age;

    public Student() { /* more code */ }
    public Student(String lastName, String firstName, int age, Sex sex) {
        this.lastName = lastName;
        this.firstName = firstName;
        this.age = age;
        this.sex = sex;
    }
    public Student(Long id, String lastName, String firstName, int age, Sex sex) {
        this.id = id;
        this.lastName = lastName;
        this.firstName = firstName;
        this.age = age;
        this.sex = sex;
    }

    public Student(String lastName, String firstName, LocalDate birthday, Sex sex, int age) {
        this.lastName = lastName;
        this.firstName = firstName;
        this.birthday = birthday;
        this.sex = sex;
        this.teacherAssessments = teacherAssessments;
        this.age = age;
    }

    // getters and setters
    @Override
    public String toString() {
        return "Student{" + "id=" + id + ", lastName=" + lastName + ", firstName=" + firstName + ", age=" + age + ", sex=" + sex + "}";
    }
}
