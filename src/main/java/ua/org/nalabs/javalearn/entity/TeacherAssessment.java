package ua.org.nalabs.javalearn.entity;

import javax.persistence.*;

@Entity
@Table(name = "assessment")
public class TeacherAssessment {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "teachassmnt_id")
    private long id;

    private String assesment;

    @ManyToOne(fetch = FetchType.LAZY)
//    @ManyToOne
    @JoinColumn (name="student_id")
//    @JsonBackReference
    private Student student;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getAssesment() {
        return assesment;
    }

    public void setAssesment(String assesment) {
        this.assesment = assesment;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }
// getters and setters
}


